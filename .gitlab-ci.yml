variables:
  BASE_IMAGE: "alpine:3.15.0"
  BUILD_IMAGE_NAME: "$CI_REGISTRY_IMAGE/branches/$CI_COMMIT_REF_SLUG-$TERRAFORM_VERSION:$CI_COMMIT_SHA"
  DOCKER_BUILDKIT: "1"
  PLATFORMS: linux/amd64,linux/arm64
  RELEASE_IMAGE_NAME: "$CI_REGISTRY_IMAGE/releases/$TERRAFORM_VERSION"
  STABLE_IMAGE_NAME: "$CI_REGISTRY_IMAGE/stable:latest"
  STABLE_VERSION: "1.1"
  TF_ADDRESS: "$CI_API_V4_URL/projects/$CI_PROJECT_ID/terraform/state/$CI_PIPELINE_IID-$STATE_NAME"

.versions:
  - TERRAFORM_BINARY_VERSION: "1.0.11"
    TERRAFORM_VERSION: "1.0"
    STATE_NAME: terraform10

# image:
#   name: registry.gitlab.com/gitlab-org/terraform-images/stable:latest

stages:          # List of stages for jobs, and their order of execution
  - validate
  - dev          # DEV environment - initial stage for development
  - qa           # QA environment - second stage for testsing
  - uat          # UAT environment - pre-production stage for final verification
  - deploy

build:
  extends: .versions
  stage: build
  services:
    - docker:20.10.12-dind
  image: docker:20.10.12-dind
  before_script:
    # Install buildx
    - mkdir -p ~/.docker/cli-plugins
    - wget https://github.com/docker/buildx/releases/download/v0.7.1/buildx-v0.7.1.linux-amd64 -O ~/.docker/cli-plugins/docker-buildx
    - chmod a+x ~/.docker/cli-plugins/docker-buildx
    # See https://www.docker.com/blog/multi-platform-docker-builds/
    - docker run --rm --privileged docker/binfmt:a7996909642ee92942dcd6cff44b9b95f08dad64
    # Registry auth
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
  script:
    - docker buildx create --use
    - docker buildx build
      --platform "$PLATFORMS"
      --build-arg BASE_IMAGE=$BASE_IMAGE
      --build-arg TERRAFORM_BINARY_VERSION=$TERRAFORM_BINARY_VERSION
      --file Dockerfile
      --tag "$BUILD_IMAGE_NAME"
      --push
      .

.test:
  image: "$BUILD_IMAGE_NAME"
  before_script:
    - gitlab-terraform version
    - jq --version
    - cd tests
  cache:
    key: "$TERRAFORM_VERSION-$CI_COMMIT_REF_SLUG"
    paths:
      - tests/.terraform/

test-init:
  extends:
    - .test
    - .versions
  stage: test-init
  script:
    - export DEBUG_OUTPUT=true
    - gitlab-terraform init

test-fmt:
  extends:
    - .test
    - .versions
  stage: test-fmt
  script:
    - gitlab-terraform fmt

test-validate:
  extends:
    - .test
    - .versions
  stage: test-validate
  script:
    - gitlab-terraform validate

test-plan:
  extends:
    - .test
    - .versions
  stage: test-plan
  script:
    - gitlab-terraform plan
    - if [[ ! -f "plan.cache" ]]; then echo "expected to find a plan.cache file"; exit 1; fi
    - gitlab-terraform plan-json
    - if [[ ! -f "plan.json" ]]; then echo "expected to find a plan.json file"; exit 1; fi
    - mv plan.cache $TERRAFORM_VERSION-plan.cache
  artifacts:
    paths:
      - "tests/*-plan.cache"

test-apply:
  extends:
    - .test
    - .versions
  stage: test-apply
  script:
    - mv $TERRAFORM_VERSION-plan.cache plan.cache
    - gitlab-terraform apply

test-destroy:
  extends:
    - .test
    - .versions
  stage: test-destroy
  script:
    - gitlab-terraform destroy
